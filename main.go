package main

import (
	"os"

	"github.com/Crocmagnon/youtubebeat/cmd"

	_ "github.com/Crocmagnon/youtubebeat/include"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
