// Config is put into a different package to prevent cyclic imports in case
// it is needed in several locations

package config

type Config struct {
	Parallelism int    `config:"parallelism"`
	MaxDepth    int    `config:"max_depth"`
	StartUrl    string `config:"start_url"`
}

var DefaultConfig = Config{
	Parallelism: 5,
	MaxDepth:    10,
	StartUrl:    "https://www.youtube.com",
}
