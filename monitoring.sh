#!/bin/bash

if (($(ps aux | grep youtubebeat | wc -l) < 2)); then
  cd /tmp/ELK/youtubebeat-6.5.0-linux-x86
  ./youtubebeat &
fi

