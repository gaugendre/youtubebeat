# Youtubebeat

Welcome to Youtubebeat.

Ensure that this folder is at the following location:
`${GOPATH}/src/github.com/Crocmagnon/youtubebeat`

## Getting Started with Youtubebeat

### Requirements

* [Golang](https://golang.org/dl/) 1.7

### Init Project
To get running with Youtubebeat and also install the
dependencies, run the following command:

```
make setup
```

It will create a clean git history for each major step. Note that you can always rewrite the history if you wish before pushing your changes.

To push Youtubebeat in the git repository, run the following commands:

```
git remote set-url origin https://github.com/Crocmagnon/youtubebeat
git push origin master
```

For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).

### Build

To build the binary for Youtubebeat run the command below. This will generate a binary
in the same directory with the name youtubebeat.

```
make
```


### Run

To run Youtubebeat with debugging output enabled, run:

```
./youtubebeat -c youtubebeat.yml -e -d "*"
```


### Test

To test Youtubebeat, run the following command:

```
make testsuite
```

alternatively:
```
make unit-tests
make system-tests
make integration-tests
make coverage-report
```

The test coverage is reported in the folder `./build/coverage/`

### Update

Each beat has a template for the mapping in elasticsearch and a documentation for the fields
which is automatically generated based on `fields.yml` by running the following command.

```
make update
```


### Cleanup

To clean  Youtubebeat source code, run the following commands:

```
make fmt
make simplify
```

To clean up the build directory and generated artifacts, run:

```
make clean
```


### Clone

To clone Youtubebeat from the git repository, run the following commands:

```
mkdir -p ${GOPATH}/src/github.com/Crocmagnon/youtubebeat
git clone https://github.com/Crocmagnon/youtubebeat ${GOPATH}/src/github.com/Crocmagnon/youtubebeat
```


For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).


## Packaging

The beat frameworks provides tools to crosscompile and package your beat for different platforms. This requires [docker](https://www.docker.com/) and vendoring as described above. To build packages of your beat, run the following command:

```
make release
```

This will fetch and create all images required for the build process. The whole process to finish can take several minutes.

## Deploy
```
env PLATFORMS='linux' make release; and scp build/distributions/youtubebeat-6.5.0-linux-x86.tar.gz REMOTE_SERVER:/tmp/ELK; and ssh REMOTE_SERVER "cd /tmp/ELK; rm -rf youtubebeat-6.5.0-linux-x86; tar xvf youtubebeat-6.5.0-linux-x86.tar.gz"
```

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂